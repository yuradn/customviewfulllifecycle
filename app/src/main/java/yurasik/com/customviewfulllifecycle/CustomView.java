package yurasik.com.customviewfulllifecycle;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;

import yurasik.com.customviewfulllifecycle.interfaces.ViewEventInterface;

/**
 * Created by Yurii on 4/11/17.
 */

public class CustomView extends View implements ViewEventInterface{
    private final static String TAG = "CustomView";
    private boolean isActive;
    private boolean isCircle;
    private int color = Color.RED;
    private final Paint paint = new Paint();

    public CustomView(Context context) {
        this(context, null);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomView,
                0, 0);

        try {
            isCircle = a.getBoolean(R.styleable.CustomView_show, false);
            color = a.getColor(R.styleable.CustomView_color_arc, Color.RED);
        } finally {
            a.recycle();
        }

        init();
    }

    private void init() {
        paint.setColor(color);
        paint.setStrokeWidth(10);
    }

    public void setShowCircle(boolean circle) {
        isCircle = circle;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(TAG, "onFinishInflate");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG, "onAttachedToWindow");
        isActive = true;
        onStart();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(TAG, "onDetachedFromWindow");
        if (isActive) {
            isActive = false;
            onStop();
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable save = super.onSaveInstanceState();
        if (isActive) {
            isActive = false;
            onStop();
        }
        Log.d(TAG, "onSaveInstanceState, args: " + save);
        return save;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState, args: " + state);
        super.onRestoreInstanceState(state);
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        Log.d(TAG, "onWindowFocusChanged, status: " + hasWindowFocus);
        super.onWindowFocusChanged(hasWindowFocus);
    }

    @Override
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        Log.d(TAG, "onStartTemporaryDetach");
    }

    @Override
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        Log.d(TAG, "onFinishTemporaryDetach");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d(TAG, "onLayout, changed: " + changed);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isActive) {
            isActive = true;
            onStart();
        }
        Log.d(TAG, "onDraw");

        if (isCircle) {
            canvas.drawCircle(getWidth()/2, getHeight()/2, 100, paint);
        }
    }

    @Override
    public void onWindowSystemUiVisibilityChanged(int visible) {
        super.onWindowSystemUiVisibilityChanged(visible);
        Log.d(TAG, "onWindowSystemUiVisibilityChanged: " + visible);
    }

    @Override
    public WindowInsets computeSystemWindowInsets(WindowInsets in, Rect outLocalInsets) {
        Log.d(TAG, "computeSystemWindowInsets: " + outLocalInsets);
        return super.computeSystemWindowInsets(in, outLocalInsets);
    }

    @Override
    public void onScreenStateChanged(int screenState) {
        super.onScreenStateChanged(screenState);
        Log.d(TAG, "onScreenStateChanged");
        switch (screenState) {
            case SCREEN_STATE_ON:
                if (!isActive && isShown()) {
                    isActive = true;
                    onStart();
                }
                break;
            case SCREEN_STATE_OFF:
                if (isActive && isShown()) {
                    isActive = false;
                    onStop();
                }
                break;
        }
    }

    @Override
    public boolean isShown() {
        return super.isShown();
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
    }
}
