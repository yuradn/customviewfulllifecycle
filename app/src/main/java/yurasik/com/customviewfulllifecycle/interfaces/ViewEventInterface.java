package yurasik.com.customviewfulllifecycle.interfaces;

/**
 * Created by Yurii on 4/11/17.
 */

public interface ViewEventInterface {
    void onStart();
    void onStop();
}
